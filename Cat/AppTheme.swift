//
//  AppTheme.swift
//  Webboard
//
//  Created by Arthit Thongpan on 3/5/17.
//  Copyright © 2017 Arthit Thongpan. All rights reserved.
//

import UIKit

class AppTheme {

    static func setApplicationAppearance() {
        setupNavigationBarAppearance()
        setupTabbar()
    }
    
    private static func setupNavigationBarAppearance() {
        let color = UIColor(red:0.17, green:0.59, blue:0.87, alpha:1)
        UINavigationBar.appearance().tintColor = color
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName: color]
    }
    
    private static func setupTabbar() {
        UITabBarItem.appearance().setTitleTextAttributes([NSFontAttributeName: UIFont.systemFont(ofSize: 13)], for: .normal)
    }
}
