//
//  ImageProfileTableViewCell.swift
//  Cat
//
//  Created by Arthit Thongpan on 5/14/17.
//  Copyright © 2017 Arthit Thongpan. All rights reserved.
//

import UIKit

class ImageProfileTableViewCell: UITableViewCell {
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var cateImageViewHeightConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setup(path: String, imageName: String) {
        profileImageView.loadImage(path: path, imageName: imageName) { (image) in
            self.updateRatio(image: image)
        }
    }
    
    func updateRatio(image: UIImage?) {
        guard let image = image else { return }
        let imageWidth = profileImageView.frame.width
        cateImageViewHeightConstraint.constant =  imageWidth * (image.size.height / image.size.width)
        self.setNeedsUpdateConstraints()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        profileImageView.image = nil
    }
}
