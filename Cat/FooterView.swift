//
//  FooterView.swift
//  Cat
//
//  Created by Arthit Thongpan on 4/30/17.
//  Copyright © 2017 Arthit Thongpan. All rights reserved.
//

import UIKit

class FooterView: UIView {

    @IBOutlet weak var containerView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundColor = UIColor.clear
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        containerView.round([.bottomLeft, .bottomRight], radius: 5.0)
    }
}
