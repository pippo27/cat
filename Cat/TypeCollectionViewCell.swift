//
//  TypeCollectionViewCell.swift
//  Cat
//
//  Created by Arthit Thongpan on 4/23/17.
//  Copyright © 2017 Arthit Thongpan. All rights reserved.
//

import UIKit

class TypeCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    func setup(path: String, type: Type) {
        titleLabel.text = type.name!
        imageView.loadImage(path: path, imageName: type.image!)
    }
}
