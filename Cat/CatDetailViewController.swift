//
//  CatDetailViewController.swift
//  Cat
//
//  Created by Arthit Thongpan on 4/23/17.
//  Copyright © 2017 Arthit Thongpan. All rights reserved.
//

import UIKit
import AMScrollingNavbar

class CatDetailViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    let sectionOfProfile = 0
    let sectionOfName = 1
    let sectionOfLocal = 2
    let sectionOfHabit = 3
    let sectionOfCharacteristic = 4
    let sectionOfCareful = 5
    let sectionOfFooter = 6
    
    let numberOfSection = 7

    var species: Species!
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let navigationController = navigationController as? ScrollingNavigationController {
            navigationController.followScrollView(tableView)
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        if let navigationController = navigationController as? ScrollingNavigationController {
            navigationController.stopFollowingScrollView()
        }
    }

    func setupTableView() {
        tableView.backgroundColor = UIColor.clear
        tableView.estimatedRowHeight = 120.0
        tableView.rowHeight = UITableViewAutomaticDimension
        
        tableView.register(UINib(nibName: "DetailTableViewCell", bundle: nil), forCellReuseIdentifier: "detail")
    }
    
    func setupSectionOfProfile(indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "profile", for: indexPath) as! CateProfileTableViewCell
        cell.setup(path: IMAGE_URL, species: species)
        return cell
    }
    
    func setupSectionOfName(indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "detail", for: indexPath) as! DetailTableViewCell
        let name = "\(species.name!) (\(species.nameEng!))"
        cell.setup(title: "ชื่อ", detail: name)
        return cell
    }
    
    func setupSectionOfLocal(indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "detail", for: indexPath) as! DetailTableViewCell
        cell.setup(title: "ถิ่นกำเนิด", detail: species.local!)
        return cell
    }
    
    func setupSectionOfHabit(indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "detail", for: indexPath) as! DetailTableViewCell
        cell.setup(title: "นิสัย", detail: species.habit!)
        return cell
    }
    
    func setupSectionOfCharacteristic(indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "detail", for: indexPath) as! DetailTableViewCell
        cell.setup(title: "ลักษณะ", detail: species.characteristic!)
        return cell
    }
    
    func setupSectionOfCareful(indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "detail", for: indexPath) as! DetailTableViewCell
        cell.setup(title: "การดูแล", detail: species.careful!)
        return cell
    }
}

// MARK: - UITableViewDataSource

extension CatDetailViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return numberOfSection
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard section == sectionOfFooter else {
            return 1
        }
        
        return 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let section = indexPath.section
        switch section {
        case sectionOfName:
            return setupSectionOfName(indexPath: indexPath)
        case sectionOfLocal:
            return setupSectionOfLocal(indexPath: indexPath)
        case sectionOfProfile:
            return setupSectionOfProfile(indexPath: indexPath)
        case sectionOfHabit:
            return setupSectionOfHabit(indexPath: indexPath)
        case sectionOfCharacteristic:
            return setupSectionOfCharacteristic(indexPath: indexPath)
        case sectionOfCareful:
            return setupSectionOfCareful(indexPath: indexPath)
        default:
            fatalError("section \(section) not found")
        }
    }
}
