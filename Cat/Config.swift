//
//  Config.swift
//  Cat
//
//  Created by Arthit Thongpan on 4/23/17.
//  Copyright © 2017 Arthit Thongpan. All rights reserved.
//


let ROOT_URL = "http://pippo27.com/cat/cellar/api/"
let IMAGE_URL = "\(ROOT_URL)images/"
let HOSPITAL_IMAGE_URL = "\(ROOT_URL)images/hospital/"
let ICON_URL = "\(ROOT_URL)images/icon/"
