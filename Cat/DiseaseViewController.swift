//
//  DiseaseViewController.swift
//  Cat
//
//  Created by Arthit Thongpan on 6/17/17.
//  Copyright © 2017 Arthit Thongpan. All rights reserved.
//

import UIKit
import SVProgressHUD

class DiseaseViewController: UIViewController {
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(handleRefresh(refreshControl:)), for: .valueChanged)
        
        return refreshControl
    }()
    
    fileprivate var diseases = [Disease]()
    lazy var visibleResults = [Disease]()
    
    /// A `nil` / empty filter string means show all results. Otherwise, show only results containing the filter.
    var filterString: String? = nil {
        didSet {
            if filterString == nil || filterString!.isEmpty {
                visibleResults = diseases
            }
            else {
                visibleResults = diseases.filter({ (disease) -> Bool in
                    return disease.name!.localizedLowercase.contains(filterString!)
                        || disease.nameEng!.localizedLowercase.contains(filterString!)
                        || disease.cause!.localizedLowercase.contains(filterString!)
                        || disease.symptom!.localizedLowercase.contains(filterString!)
                        || disease.treatment!.localizedLowercase.contains(filterString!)
                })
            }
            
            tableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        loadDiseases()
    }
    
    func setupTableView() {
        tableView.backgroundColor = UIColor.clear
        tableView.estimatedRowHeight = 60.0
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.refreshControl = refreshControl
    }
    
    // MARK: - Refresh Control
    
    @objc func handleRefresh(refreshControl: UIRefreshControl) {
        loadDiseases()
    }
    
    // MARK: - Webservice
    
    func loadDiseases() {
        SVProgressHUD.show()
        ApiConnector.getDiseases { result in
            self.refreshControl.endRefreshing()
            SVProgressHUD.dismiss()
            switch result {
            case let .success(diseases):
                self.diseases = diseases
                self.visibleResults = diseases
                self.filterString = self.searchBar.text
                self.tableView.reloadData()
            case let .fail(error):
                SVProgressHUD.showError(withStatus: error)
                break
            }
        }
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let identifier = segue.identifier else { return }
        
        switch identifier {
        case "detail":
            let diseaseDetailViewController = segue.destination as! DiseaseDetailViewController
            diseaseDetailViewController.disease = sender as! Disease
        default:
            break
        }
    }
    
    // MARK: - ScrollView
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        view.endEditing(true)
    }
}

// MARK: - UISearchBarDelegate

extension DiseaseViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        print("The default search selected scope button index changed to \(selectedScope).")
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        var searchText = ""
        if let text = searchBar.text {
            searchText = text
        }
        print("The default search bar keyboard search button was tapped: \(searchText).")
        
        searchBar.resignFirstResponder()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        print("The default search bar cancel button was tapped.")
        
        searchBar.text = ""
        filterString = ""
        searchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        print("searchText = \(searchText)")
        filterString = searchText.localizedLowercase
    }
}

// MARK: - UITableViewDataSource & UITableViewDelegate

extension DiseaseViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return visibleResults.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let disease = visibleResults[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! SimpleTableViewCell
        cell.titleLabel.text = disease.name
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let disease = visibleResults[indexPath.row]
        tableView.deselectRow(at: indexPath, animated: true)
        performSegue(withIdentifier: "detail", sender: disease)
    }
}
