//
//  HospitalMapViewController.swift
//  Cat
//
//  Created by Arthit Thongpan on 5/14/17.
//  Copyright © 2017 Arthit Thongpan. All rights reserved.
//

import UIKit
import MapKit
import SVProgressHUD

class HospitalMapViewController: UIViewController {

    @IBOutlet weak var mapView: MKMapView!
    fileprivate var hospitals = [Hospital]()
    fileprivate var annotations = [CustomAnnotation]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadHospitals()
    }
    
    func updateMapView() {
        for hospital in hospitals {
           if hospital.latitude == 0 || hospital.longitude == 0 { continue }
            
            let location = CLLocationCoordinate2D(latitude: hospital.latitude!, longitude: hospital.longitude!)
            let customAnnotation = CustomAnnotation(title: hospital.name!, locationName: hospital.address!, coordinate: location)
            annotations.append(customAnnotation)
        }

        if let annotation = annotations.last {
            let span = MKCoordinateSpanMake(0.05, 0.05)
            let region = MKCoordinateRegion(center: annotation.coordinate, span: span)
            mapView.setRegion(region, animated: true)
        }
       
        mapView.addAnnotations(annotations)
    }
    
    func openMapForPlace(hospital: Hospital) {
        if hospital.latitude == nil || hospital.longitude == nil { return }
        
        let latitude: CLLocationDegrees = hospital.latitude!
        let longitude: CLLocationDegrees = hospital.longitude!
        
        let regionDistance:CLLocationDistance = 0.05
        let coordinates = CLLocationCoordinate2DMake(latitude, longitude)
        let regionSpan = MKCoordinateRegionMakeWithDistance(coordinates, regionDistance, regionDistance)
        let options = [
            MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
            MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
        ]
        let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = hospital.name!
        mapItem.openInMaps(launchOptions: options)
    }
    
    // MARK: - Webservice
    
    func loadHospitals() {
        SVProgressHUD.show()
        ApiConnector.getHospitals { result in
            SVProgressHUD.dismiss()
           
            switch result {
            case let .success(hospitals):
                self.hospitals = hospitals
                self.updateMapView()
            case let .fail(error):
                SVProgressHUD.showError(withStatus: error)
                break
            }
        }
    }
}

// MARK: - MKMapViewDelegate

extension HospitalMapViewController: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if let annotation = annotation as? CustomAnnotation {
            let identifier = "pin"
            var view: MKPinAnnotationView
            if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
                as? MKPinAnnotationView {
                dequeuedView.annotation = annotation
                view = dequeuedView
            } else {
                
                view = MKPinAnnotationView(annotation: annotation, reuseIdentifier: identifier)
                view.canShowCallout = true
                view.calloutOffset = CGPoint(x: -5, y: 5)
                view.rightCalloutAccessoryView = UIButton(type: .detailDisclosure) as UIView
            }
            
            return view
        }
        
        return nil
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        let location = view.annotation as! CustomAnnotation
        let launchOptions = [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving]
        location.mapItem().openInMaps(launchOptions: launchOptions)
    }
}
