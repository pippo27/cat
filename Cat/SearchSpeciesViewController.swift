//
//  SearchSpeciesViewController.swift
//  Cat
//
//  Created by Arthit Thongpan on 4/23/17.
//  Copyright © 2017 Arthit Thongpan. All rights reserved.
//

import UIKit
import SVProgressHUD

class SearchSpeciesViewController: UIViewController {

    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var collectionView: UICollectionView!
    static func instantiate() -> SearchSpeciesViewController {
        return UIStoryboard(name: "Search", bundle: nil).instantiateViewController(withIdentifier: "SearchSpeciesViewController") as! SearchSpeciesViewController
    }
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
//        refreshControl.addTarget(self, action: #selector(handleRefresh(refreshControl:)), for: .valueChanged)
        refreshControl.addTarget(self, action: #selector(handleRefresh(refreshControl:)), for: .valueChanged)
        
        return refreshControl
    }()
        
    fileprivate var speciesList = [Species]()
    
    lazy var visibleResults = [Species]()
    
    /// A `nil` / empty filter string means show all results. Otherwise, show only results containing the filter.
    var filterString: String? = nil {
        didSet {
            if filterString == nil || filterString!.isEmpty {
                visibleResults = speciesList
            }
            else {
   
                visibleResults = speciesList.filter({ (species) -> Bool in
                    return species.name!.localizedLowercase.contains(filterString!)
                    || species.nameEng!.localizedLowercase.contains(filterString!)
                    || species.local!.localizedLowercase.contains(filterString!)
                    || species.habit!.localizedLowercase.contains(filterString!)
                    || species.characteristic!.localizedLowercase.contains(filterString!)
                    
                })
            }
            
            collectionView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupCollectionView()
        loadCats()
    }
    
    func setupCollectionView() {
        collectionView.refreshControl = refreshControl
    }
    
    func setupSearchController() {
        //        searchBar.showsCancelButton = true
        searchBar.delegate = self
        searchBar.showsScopeBar = true
    }
    
    // MARK: - Refresh Control
    
    @objc func handleRefresh(refreshControl: UIRefreshControl) {
        loadCats()
    }
    
    // MARK: - Webservice
    
    func loadCats() {
        SVProgressHUD.show()
        ApiConnector.getSpecies { result in
            self.refreshControl.endRefreshing()
            SVProgressHUD.dismiss()
            switch result {
            case let .success(speciesList):
                self.speciesList = speciesList
                self.visibleResults = speciesList
                self.filterString = self.searchBar.text
                self.collectionView.reloadData()
            case let .fail(error):
                SVProgressHUD.showError(withStatus: error)
                break
            }
        }
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let identifier = segue.identifier else { return }
        
        switch identifier {
        case "detail":
            let catDetailViewController = segue.destination as! CatDetailViewController
            catDetailViewController.species = sender as! Species
        default:
            break
        }
    }
    
    // MARK: - ScrollView
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        view.endEditing(true)
    }
}

// MARK: - UISearchBarDelegate

extension SearchSpeciesViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        print("The default search selected scope button index changed to \(selectedScope).")
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        var searchText = ""
        if let text = searchBar.text {
            searchText = text
        }
        print("The default search bar keyboard search button was tapped: \(searchText).")
        
        searchBar.resignFirstResponder()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        print("The default search bar cancel button was tapped.")
        
        searchBar.text = ""
        filterString = ""
        searchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        print("searchText = \(searchText)")
        filterString = searchText.localizedLowercase
    }
}

// MARK: - UICollectionViewDelegate & UICollectionViewDataSource

extension SearchSpeciesViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return visibleResults.count
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        switch kind {
        case UICollectionElementKindSectionHeader:
            let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "header", for: indexPath) as! HeaderCollectionReusableView
            return header
        case UICollectionElementKindSectionFooter:
            let footer = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "footer", for: indexPath) as! FooterCollectionReusableView
            return footer
        default:
            fatalError("\(kind) not fond")
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "species", for: indexPath) as! SpeciesCollectionViewCell
        let species = visibleResults[indexPath.item]
        cell.setup(path: IMAGE_URL, species: species)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let species = visibleResults[indexPath.item]
        performSegue(withIdentifier: "detail", sender: species)
    }
}

// MARK: - UICollectionViewDelegateFlowLayout

extension SearchSpeciesViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: 20)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: 20)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        guard let flowLayout = collectionViewLayout as? UICollectionViewFlowLayout else {
            return CGSize()
        }
        
        // subtract section left/ right insets mentioned in xib view
//        let width =  (collectionView.frame.width - flowLayout.sectionInset.left - flowLayout.sectionInset.right)
        return CGSize(width: collectionView.frame.width , height: (flowLayout.itemSize.height))
    }
}

