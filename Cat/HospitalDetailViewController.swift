//
//  HospitalDetailViewController.swift
//  Cat
//
//  Created by Arthit Thongpan on 5/14/17.
//  Copyright © 2017 Arthit Thongpan. All rights reserved.
//

import UIKit
import MapKit

class HospitalDetailViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    
    var hospital: Hospital!
    
    let sectionOfImageProfile = 0
    let sectionOfName = 1
    let sectionOfAddress = 2
    let sectionOfPhone = 3
    let sectionOfOpeningTimes = 4
    let sectionOfWebsite = 5
    
    let numberOfSection = 6
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
    }
    
    func setupTableView() {
        tableView.backgroundColor = UIColor.clear
        tableView.estimatedRowHeight = 120.0
        tableView.rowHeight = UITableViewAutomaticDimension
        
        tableView.register(UINib(nibName: "DetailTableViewCell", bundle: nil), forCellReuseIdentifier: "detail")
        tableView.register(UINib(nibName: "AddressTableViewCell", bundle: nil), forCellReuseIdentifier: "address")
    }
    
    
    func openMapForPlace(hospital: Hospital) {
        if hospital.latitude == 0 || hospital.longitude == 0 { return }
        
        let latitude: CLLocationDegrees = hospital.latitude!
        let longitude: CLLocationDegrees = hospital.longitude!
        
        let regionDistance:CLLocationDistance = 0.05
        let coordinates = CLLocationCoordinate2DMake(latitude, longitude)
        let regionSpan = MKCoordinateRegionMakeWithDistance(coordinates, regionDistance, regionDistance)
        let options = [
            MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
            MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
        ]
        let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = hospital.name!
        mapItem.openInMaps(launchOptions: options)
    }
    
    func setupSectionOfProfile(indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "image_profile", for: indexPath) as! ImageProfileTableViewCell
        cell.setup(path: HOSPITAL_IMAGE_URL, imageName: hospital.image!)
        return cell
    }
    
    func setupDetailCell(indexPath: IndexPath, title: String, detail: String) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "detail", for: indexPath) as! DetailTableViewCell
        cell.setup(title: title, detail: detail)
        return cell
    }
    
    func setupAddressCell(indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "address", for: indexPath) as! AddressTableViewCell
        cell.setup(hospital: hospital)
        return cell
    }
}

extension HospitalDetailViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return numberOfSection
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case sectionOfImageProfile:
            return (hospital.image?.isEmpty)! ? 0 : 1
        case sectionOfName:
            return 1
        case sectionOfAddress:
            return 1
        case sectionOfPhone:
            return (hospital.phone == nil || hospital.phone == "") ? 0 : 1
        case sectionOfOpeningTimes:
            return (hospital.openingTimes == nil || hospital.openingTimes == "") ? 0 : 1
        case sectionOfWebsite:
            return (hospital.website == nil || hospital.website == "") ? 0 : 1
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
        let section = indexPath.section
        switch section {
        case sectionOfImageProfile:
            return setupSectionOfProfile(indexPath: indexPath)
        case sectionOfName:
            return setupDetailCell(indexPath: indexPath, title: "โรงพยาบล", detail: hospital.name!)
        case sectionOfAddress:
            if hospital.latitude != 0 && hospital.longitude != 0 {
                return setupAddressCell(indexPath: indexPath)
            } else {
                return setupDetailCell(indexPath: indexPath, title: "ที่อยู่", detail: hospital.address!)
            }
            
        case sectionOfPhone:
            return setupDetailCell(indexPath: indexPath, title: "โทรศัพท์", detail: hospital.phone!)
        case sectionOfOpeningTimes:
            return setupDetailCell(indexPath: indexPath, title: "เวลาทำการ", detail: hospital.openingTimes!)
        case sectionOfWebsite:
            return setupDetailCell(indexPath: indexPath, title: "เว็บไซต์", detail: hospital.website!)
        default:
            fatalError("section \(section) not found")
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard indexPath.section == sectionOfAddress else {
            return
        }
        
        openMapForPlace(hospital: hospital)
    }
}
