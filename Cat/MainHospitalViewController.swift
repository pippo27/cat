//
//  MainHospitalViewController.swift
//  Cat
//
//  Created by Arthit Thongpan on 5/14/17.
//  Copyright © 2017 Arthit Thongpan. All rights reserved.
//

import UIKit

class MainHospitalViewController: UIViewController {

    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var containerHospitalVC: UIView!
    @IBOutlet weak var containerHopitalMapVC: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateView(index: 0)
    }
    
    func updateView(index: Int) {
        if index == 0 {
            self.containerHospitalVC.isHidden = false
            self.containerHopitalMapVC.isHidden = true
        } else {
            self.containerHospitalVC.isHidden = true
            self.containerHopitalMapVC.isHidden = false
        }
        
        
//        if index == 0 {
//            UIView.animate(withDuration: 0.5, animations: {
//                self.containerHospitalVC.alpha = 1
//                self.containerHopitalMapVC.alpha = 0
//            })
//            
//        } else {
//            UIView.animate(withDuration: 0.5, animations: {
//                self.containerHospitalVC.alpha = 0
//                self.containerHopitalMapVC.alpha = 1
//            })
//        }
    }
   
    // MARK: - Actions
    
    @IBAction func changedTab(_ sender: UISegmentedControl) {
        updateView(index: sender.selectedSegmentIndex)
    }
}
