//
//  UIImageView+Extension.swift
//  Cat
//
//  Created by Arthit Thongpan on 4/23/17.
//  Copyright © 2017 Arthit Thongpan. All rights reserved.
//

import UIKit
import SDWebImage

extension UIImageView {
    
    func loadImage(path:String, imageName: String, completion: ((UIImage?) -> Void)? = nil) {

        
        let fullPath : String = "\(path)\(imageName)"
        let urlStr : String = fullPath.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        let placeholderImage = UIImage(named: "placeholder")
        guard let url = URL(string: urlStr) else {
            print("image not found: \(urlStr)")
            return
        }
        
        self.sd_setImage(with: url,
                         placeholderImage: placeholderImage,
                         options: [])
        { (image, error, cacheType, url) in
            let isFadeIn = (cacheType == SDImageCacheType.none)
            self.fadeIn(isFadeIn: isFadeIn)
            completion?(image)
        }
    }
    
    func fadeIn(isFadeIn: Bool = true) {
        if isFadeIn {
            let transition = CATransition()
            transition.type = kCATransitionFade
            transition.duration = 0.34
            transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)
            self.layer.add(transition, forKey: nil)
        }
    }
}
