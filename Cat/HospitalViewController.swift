//
//  HospitalViewController.swift
//  Cat
//
//  Created by Arthit Thongpan on 5/14/17.
//  Copyright © 2017 Arthit Thongpan. All rights reserved.
//

import UIKit
import SVProgressHUD

class HospitalViewController: UIViewController {
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(handleRefresh(refreshControl:)), for: .valueChanged)
        
        return refreshControl
    }()
    
    fileprivate var hospitals = [Hospital]()
    lazy var visibleResults = [Hospital]()
    
    /// A `nil` / empty filter string means show all results. Otherwise, show only results containing the filter.
    var filterString: String? = nil {
        didSet {
            if filterString == nil || filterString!.isEmpty {
                visibleResults = hospitals
            }
            else {
                
                visibleResults = hospitals.filter({ (hospital) -> Bool in
                    return hospital.name!.localizedLowercase.contains(filterString!)
                        || hospital.address!.localizedLowercase.contains(filterString!)
                        || hospital.phone!.localizedLowercase.contains(filterString!)
                        || hospital.website!.localizedLowercase.contains(filterString!)
                })
            }
            
            tableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        loadHospitals()
    }
    
    func setupTableView() {
        tableView.backgroundColor = UIColor.clear
        tableView.estimatedRowHeight = 44.0
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.refreshControl = refreshControl
    }
    
    // MARK: - Refresh Control
    
    @objc func handleRefresh(refreshControl: UIRefreshControl) {
        loadHospitals()
    }
    
    // MARK: - Webservice
    
    func loadHospitals() {
        SVProgressHUD.show()
        ApiConnector.getHospitals { result in
            SVProgressHUD.dismiss()
            self.refreshControl.endRefreshing()
            switch result {
            case let .success(hospitals):
                self.hospitals = hospitals
                self.visibleResults = hospitals
                self.filterString = self.searchBar.text
                self.tableView.reloadData()
            case let .fail(error):
                SVProgressHUD.showError(withStatus: error)
                break
            }
        }
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let identifier = segue.identifier else { return }
        
        switch identifier {
        case "detail":
            let hospitalDetailViewController = segue.destination as! HospitalDetailViewController
            hospitalDetailViewController.hospital = sender as! Hospital
        default:
            break
        }
    }
    
    // MARK: - ScrollView
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        view.endEditing(true)
    }
}

// MARK: - UISearchBarDelegate

extension HospitalViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        print("The default search selected scope button index changed to \(selectedScope).")
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        var searchText = ""
        if let text = searchBar.text {
            searchText = text
        }
        print("The default search bar keyboard search button was tapped: \(searchText).")
        
        searchBar.resignFirstResponder()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        print("The default search bar cancel button was tapped.")
        
        searchBar.text = ""
        filterString = ""
        searchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        print("searchText = \(searchText)")
        filterString = searchText.localizedLowercase
    }
}

// MARK: - UITableViewDataSource & UITableViewDelegate 

extension HospitalViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return visibleResults.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let hospital = visibleResults[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! SimpleTableViewCell
        cell.titleLabel.text = hospital.name
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let hospital = visibleResults[indexPath.row]
        tableView.deselectRow(at: indexPath, animated: true)
        performSegue(withIdentifier: "detail", sender: hospital)
    }
}
