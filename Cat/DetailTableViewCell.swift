//
//  DetailTableViewCell.swift
//  Cat
//
//  Created by Arthit Thongpan on 4/23/17.
//  Copyright © 2017 Arthit Thongpan. All rights reserved.
//

import UIKit

class DetailTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var detailTextView: UITextView!
    
    func setup(title: String, detail: String) {
        titleLabel.text = title
        detailTextView.text = detail
    }
}
