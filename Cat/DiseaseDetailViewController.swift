//
//  DiseaseDetailViewController.swift
//  Cat
//
//  Created by Arthit Thongpan on 6/17/17.
//  Copyright © 2017 Arthit Thongpan. All rights reserved.
//

import UIKit

class DiseaseDetailViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    // TableView Section
    let sectionOfName = 0
    let sectionOfCause = 1
    let sectionOfSymptom = 2
    let sectionOfTreatment = 3
    let numberOfSection = 4
    
    var disease: Disease!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
    }
    
    func setupTableView() {
        tableView.backgroundColor = UIColor.clear
        tableView.estimatedRowHeight = 120.0
        tableView.rowHeight = UITableViewAutomaticDimension
        
        tableView.register(UINib(nibName: "DetailTableViewCell", bundle: nil), forCellReuseIdentifier: "detail")
    }
    
    func setupSectionOfName(indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "detail", for: indexPath) as! DetailTableViewCell
        let name = "\(disease.name!) (\(disease.nameEng!))"
        cell.setup(title: "โรค", detail: name)
        return cell
    }
    
    func setupCell(indexPath: IndexPath, title: String, detail: String) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "detail", for: indexPath) as! DetailTableViewCell
        cell.setup(title: title, detail: detail)
        return cell
    }
}

// MARK: - UITableViewDataSource

extension DiseaseDetailViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return numberOfSection
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let section = indexPath.section
        switch section {
        case sectionOfName:
            return setupSectionOfName(indexPath: indexPath)
        case sectionOfCause:
            return setupCell(indexPath: indexPath, title: "สาเหตุการเกิดโรค", detail: disease.cause!)
        case sectionOfSymptom:
            return setupCell(indexPath: indexPath, title: "อาการ", detail: disease.symptom!)
        case sectionOfTreatment:
            return setupCell(indexPath: indexPath, title: "การดูแลรักษา", detail: disease.treatment!)
        default:
            fatalError("section \(section) not found")
        }
    }
}
