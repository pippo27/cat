//
//  TypeViewController.swift
//  Cat
//
//  Created by Arthit Thongpan on 4/22/17.
//  Copyright © 2017 Arthit Thongpan. All rights reserved.
//

import UIKit
import SVProgressHUD

class TypeViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(handleRefresh(refreshControl:)), for: .valueChanged)
        
        return refreshControl
    }()
    
    fileprivate var types = [Type]()
    override func viewDidLoad() {
        super.viewDidLoad()
        setupCollectionView()
        loadType()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    func setupCollectionView() {
        collectionView.addSubview(refreshControl)
    }
    
    // MARK: - Refresh Control
    
    @objc func handleRefresh(refreshControl: UIRefreshControl) {
        loadType()
    }
    
    // MARK: - Webservice
    
    func loadType() {
        SVProgressHUD.show()
        ApiConnector.getTypes { result in
            self.refreshControl.endRefreshing()
            SVProgressHUD.dismiss()
            switch result {
            case let .success(types):
                self.types = types
                self.collectionView.reloadData()
            case let .fail(error):
                SVProgressHUD.showError(withStatus: error)
                break
            }
        }
    }
}

// MARK: - UICollectionViewDelegate & UICollectionViewDataSource

extension TypeViewController: UICollectionViewDelegate, UICollectionViewDataSource {
   
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return types.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "type", for: indexPath) as! TypeCollectionViewCell
        let type = types[indexPath.item]
        cell.setup(path: ICON_URL, type: type)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("Collection view at row \(collectionView.tag) selected index path \(indexPath)")
    }
}

// MARK: - UICollectionViewDelegateFlowLayout

extension TypeViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        guard let flowLayout = collectionViewLayout as? UICollectionViewFlowLayout else {
            return CGSize()
        }

        // subtract section left/ right insets mentioned in xib view
        let width =  (collectionView.frame.width - flowLayout.sectionInset.left - flowLayout.sectionInset.right)
        return CGSize(width: CGFloat(width), height: (flowLayout.itemSize.height))
    }
}
