//
//  MainNavigationViewController.swift
//  Cat
//
//  Created by Arthit Thongpan on 6/15/17.
//  Copyright © 2017 Arthit Thongpan. All rights reserved.
//

import UIKit

class MainNavigationViewController: UINavigationController {
    
    var homeButtonItem: UIBarButtonItem {
        let rightBarItem = UIBarButtonItem(title: "Menu", style: .plain, target: self, action: #selector(gotoMainMenu(item:)))
        return rightBarItem
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        delegate = self
    }
    
    @objc func gotoMainMenu(item: UIBarButtonItem) {
        popToRootViewController(animated: true)
    }
}

// MARK: - UINavigationControllerDelegate

extension MainNavigationViewController: UINavigationControllerDelegate {
    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
        if viewController is MainMenuViewController {
            viewController.navigationItem.rightBarButtonItem = nil
        } else {
            if viewController.navigationItem.rightBarButtonItem == nil {
                viewController.navigationItem.rightBarButtonItem = homeButtonItem
            }
        }
    }
}

