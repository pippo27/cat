/* 
Copyright (c) 2017 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

import Foundation
 
/* For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar */

public class Species {
	public var id : Int?
	public var name : String?
	public var nameEng : String?
	public var local : String?
	public var type : String?
	public var habit : String?
	public var characteristic : String?
	public var careful : String?
	public var image : String?

/**
    Returns an array of models based on given dictionary.
    
    Sample usage:
    let species_list = Species.modelsFromDictionaryArray(someDictionaryArrayFromJSON)

    - parameter array:  NSArray from JSON dictionary.

    - returns: Array of Species Instances.
*/
    public class func modelsFromDictionaryArray(array:NSArray) -> [Species]
    {
        var models:[Species] = []
        for item in array
        {
            models.append(Species(dictionary: item as! NSDictionary)!)
        }
        return models
    }

/**
    Constructs the object based on the given dictionary.
    
    Sample usage:
    let species = Species(someDictionaryFromJSON)

    - parameter dictionary:  NSDictionary from JSON.

    - returns: Species Instance.
*/
	required public init?(dictionary: NSDictionary) {

		id = dictionary["id"] as? Int
		name = dictionary["name"] as? String
		nameEng = dictionary["nameEng"] as? String
		local = dictionary["local"] as? String
		type = dictionary["type"] as? String
		habit = dictionary["habit"] as? String
		characteristic = dictionary["characteristic"] as? String
		careful = dictionary["careful"] as? String
		image = dictionary["image"] as? String
	}

		
/**
    Returns the dictionary representation for the current instance.
    
    - returns: NSDictionary.
*/
	public func dictionaryRepresentation() -> NSDictionary {

		let dictionary = NSMutableDictionary()

		dictionary.setValue(self.id, forKey: "id")
		dictionary.setValue(self.name, forKey: "name")
		dictionary.setValue(self.nameEng, forKey: "nameEng")
		dictionary.setValue(self.local, forKey: "local")
		dictionary.setValue(self.type, forKey: "type")
		dictionary.setValue(self.habit, forKey: "habit")
		dictionary.setValue(self.characteristic, forKey: "characteristic")
		dictionary.setValue(self.careful, forKey: "careful")
		dictionary.setValue(self.image, forKey: "image")

		return dictionary
	}

}
