/* 
Copyright (c) 2017 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

import Foundation
 
/* For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar */

public class Hospital {
	public var id : Int?
	public var name : String?
	public var address : String?
	public var phone : String?
	public var openingTimes : String?
    public var website: String?
	public var latitude : Double?
	public var longitude : Double?
	public var image : String?

/**
    Returns an array of models based on given dictionary.
    
    Sample usage:
    let Hospital_list = Hospital.modelsFromDictionaryArray(someDictionaryArrayFromJSON)

    - parameter array:  NSArray from JSON dictionary.

    - returns: Array of Hospital Instances.
*/
    public class func modelsFromDictionaryArray(array:NSArray) -> [Hospital]
    {
        var models:[Hospital] = []
        for item in array
        {
            models.append(Hospital(dictionary: item as! NSDictionary)!)
        }
        return models
    }

/**
    Constructs the object based on the given dictionary.
    
    Sample usage:
    let Hospital = Hospital(someDictionaryFromJSON)

    - parameter dictionary:  NSDictionary from JSON.

    - returns: Hospital Instance.
*/
	required public init?(dictionary: NSDictionary) {

		id = dictionary["id"] as? Int
		name = dictionary["name"] as? String
		address = dictionary["address"] as? String
		phone = dictionary["phone"] as? String
		openingTimes = dictionary["OpeningTimes"] as? String
        website = dictionary["website"] as? String
		latitude = Double((dictionary["latitude"] as? String)!)
		longitude = Double((dictionary["longitude"] as? String)!)
		image = dictionary["image"] as? String
	}

		
/**
    Returns the dictionary representation for the current instance.
    
    - returns: NSDictionary.
*/
	public func dictionaryRepresentation() -> NSDictionary {

		let dictionary = NSMutableDictionary()

		dictionary.setValue(self.id, forKey: "id")
		dictionary.setValue(self.name, forKey: "name")
		dictionary.setValue(self.address, forKey: "address")
		dictionary.setValue(self.phone, forKey: "phone")
		dictionary.setValue(self.openingTimes, forKey: "OpeningTimes")
        dictionary.setValue(self.website, forKey: "website")
		dictionary.setValue(self.latitude, forKey: "latitude")
		dictionary.setValue(self.longitude, forKey: "longitude")
		dictionary.setValue(self.image, forKey: "image")

		return dictionary
	}

}
