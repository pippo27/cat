//
//  ApiConnector.swift
//  Cat
//
//  Created by Arthit Thongpan on 4/22/17.
//  Copyright © 2017 Arthit Thongpan. All rights reserved.
//

import Alamofire

enum ApiResult<Type> {
    case success(Type)
    case fail(String)
}

class ApiConnector {
    
    private static let END_POINT = "\(ROOT_URL)index.php/"
    private static let GET_TYPES_API = "types"
    private static let GET_SPECIES_API = "species/%d"
    private static let GET_HOSPITALS_API = "hospitals"
    private static let GET_DISEASES_API = "diseases";
    
    // MARK: - Disease
    
    static func getDiseases(completion: @escaping(ApiResult<[Disease]>) -> Void) {
        let url = URL(string: "\(END_POINT)\(GET_DISEASES_API)")
        let method = HTTPMethod.get
        let params: [String: AnyObject]? = nil
        
        connectApi(url: url!, method: method, parameters: params) { result in
            switch result {
            case let .success(json):
                let jsonDict = json as! NSDictionary
                let diseases: [Disease] = Disease.modelsFromDictionaryArray(array: jsonDict["diseases"] as! NSArray)
                completion(.success(diseases))
            case let .fail(error):
                completion(.fail(error))
            }
        }
    }
    
    // MARK: - Type
    
    static func getTypes(completion: @escaping (ApiResult<[Type]>) -> Void) {
        let url = URL(string: "\(END_POINT)\(GET_TYPES_API)")
        let method = HTTPMethod.get
        let params: [String: AnyObject]? = nil
        
        connectApi(url: url!, method: method, parameters: params) { result in
            switch result {
            case let .success(json):
                let jsonDict = json as! NSDictionary
                let types: [Type] = Type.modelsFromDictionaryArray(array: jsonDict["types"] as! NSArray)
                completion(.success(types))
            case let .fail(error):
                completion(.fail(error))
            }
        }
    }
    
    // MARK: - Species
    
    static func getSpecies(typeId: Int = 0 , completion: @escaping (ApiResult<[Species]>) -> Void) {
        let urlString = "\(END_POINT)\(String(format: GET_SPECIES_API, arguments: [typeId]))"
        let url = URL(string: urlString)
        let method = HTTPMethod.get
        let params: [String: AnyObject]? = nil
        
        connectApi(url: url!, method: method, parameters: params) { result in
            switch result {
            case let .success(json):
                let jsonDict = json as! NSDictionary
                let speciesList: [Species] = Species.modelsFromDictionaryArray(array: jsonDict["species"] as! NSArray)
                completion(.success(speciesList))
            case let .fail(error):
                completion(.fail(error))
            }
        }
    }
    
    // MARK: - Hospitals
    
    static func getHospitals(completion: @escaping (ApiResult<[Hospital]>) -> Void) {
        let url = URL(string: "\(END_POINT)\(GET_HOSPITALS_API)")
        let method = HTTPMethod.get
        let params: [String: AnyObject]? = nil
        
        connectApi(url: url!, method: method, parameters: params) { result in
            switch result {
            case let .success(json):
                let jsonDict = json as! NSDictionary
                let hospitals: [Hospital] = Hospital.modelsFromDictionaryArray(array: jsonDict["hospitals"] as! NSArray)
                completion(.success(hospitals))
            case let .fail(error):
                completion(.fail(error))
            }
        }
    }
    
    // MARK: - Private
    
    private static func connectApi(url: URL, method: HTTPMethod, parameters: [String: AnyObject]?, completion: @escaping (ApiResult<Any?>) -> Void) {
        
        Alamofire.request(url, method: method, parameters: parameters, encoding: JSONEncoding.default, headers: nil)
            .responseJSON { response in
                print(response.request as Any)  // original URL request
                print(response.response as Any) // URL response
                print(response.result.value as Any)   // result of response serialization
                
                handleResponse(response: response, completion: completion)
        }
    }
    
    private static func handleResponse(response: (DataResponse<Any>), completion: (ApiResult<Any?>) -> Void) {
        if response.response == nil {
            completion(.fail("Cannot connect to server"))
            return
        }
        
        print("response code = \(response.response!.statusCode)")
        
        switch response.response!.statusCode {
        case 200:
            completion(.success(response.result.value))
        case 401:
            // Authorization Fail
            let defaultError = "Unexpected error has occured"
            completion(.fail(defaultError))
        case 404:
            completion(.fail("Api not found"))
            
        case 503:
            completion(.fail("Service is currently unavailable! Please try again later"))
            
        default:
            let defaultError = "Unexpected error has occured"
            completion(.fail(defaultError))
        }
    }
}
