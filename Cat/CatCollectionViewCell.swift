//
//  CatCollectionViewCell.swift
//  Cat
//
//  Created by Arthit Thongpan on 4/23/17.
//  Copyright © 2017 Arthit Thongpan. All rights reserved.
//

import UIKit
import SDWebImage

class CatCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    func setup(path: String, species: Species) {
        titleLabel.text = species.name!
        imageView.loadImage(path: path, imageName: species.image!) { (image) in
            
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        imageView.image = nil
    }
}
