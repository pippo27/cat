//
//  CatViewController.swift
//  Cat
//
//  Created by Arthit Thongpan on 4/22/17.
//  Copyright © 2017 Arthit Thongpan. All rights reserved.
//

import UIKit
import SVProgressHUD

class CatViewController: UIViewController {
    
    static func instantiate() -> CatViewController {
        return UIStoryboard(name: "Cat", bundle: nil).instantiateViewController(withIdentifier: "CatViewController") as! CatViewController
    }
    
    @IBOutlet weak var collectionView: UICollectionView!
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(handleRefresh(refreshControl:)), for: .valueChanged)
        
        return refreshControl
    }()
    
    var typeId: Int!
    fileprivate var speciesList = [Species]()
    fileprivate var imageRatio: [Int: CGFloat] = [Int: CGFloat]()
    
    var column: CGFloat! {
        get {
            return UIDevice.current.userInterfaceIdiom == .phone ? 2 : 3
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupCollectionView()
        
        loadCats()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }

    func setupCollectionView() {
        let layout = CollectionViewWaterfallLayout()
        layout.sectionInset = UIEdgeInsetsMake(10, 10, 10, 10)
        collectionView.collectionViewLayout = layout
        collectionView.refreshControl = refreshControl
    }
    
    func setupCatCell(indexPath: IndexPath) -> UICollectionViewCell {
        let species = speciesList[indexPath.item]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cat", for: indexPath) as! CatCollectionViewCell
        cell.tag = indexPath.row
        cell.setup(path: IMAGE_URL, species: species)
        return cell
    }
    
    // MARK: - Refresh Control
    
    @objc func handleRefresh(refreshControl: UIRefreshControl) {
        loadCats()
    }
    
    // MARK: - Webservice
    
    func loadCats() {
        SVProgressHUD.show()
        ApiConnector.getSpecies() { result in
            self.refreshControl.endRefreshing()
            SVProgressHUD.dismiss()
            switch result {
            case let .success(speciesList):
                self.speciesList = speciesList
                self.collectionView.reloadData()
            case let .fail(error):
                SVProgressHUD.showError(withStatus: error)
                break
            }
        }
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let identifier = segue.identifier else { return }
        
        switch identifier {
        case "detail":
            guard let indexPath = collectionView.indexPathsForSelectedItems?.first else { return }
            let species = speciesList[indexPath.item]
            let catDetailViewController = segue.destination as! CatDetailViewController
            catDetailViewController.species = species
        default:
            break
        }
    }
}

// MARK: - CollectionViewWaterfallLayoutDelegate & UICollectionViewDataSource

extension CatViewController: CollectionViewWaterfallLayoutDelegate, UICollectionViewDataSource {
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, columnCountForSection section: NSInteger) -> NSInteger {
        return NSInteger(column)
    }
    
    func collectionView(collectionView: UICollectionView, layout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
        guard let layout = layout as? CollectionViewWaterfallLayout else {
            return CGSize()
        }
        
        let widthAvailbleForAllItems =  (collectionView.frame.width - layout.sectionInset.left - layout.sectionInset.right)
        let widthForOneItem = (widthAvailbleForAllItems - (CGFloat(column - 1) * CGFloat(layout.minimumInteritemSpacing))) / column
        return CGSize(width: CGFloat(widthForOneItem), height: widthForOneItem + 37)
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return speciesList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return setupCatCell(indexPath: indexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        performSegue(withIdentifier: "detail", sender: self)
    }
}


