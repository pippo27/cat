//
//  CateProfileTableViewCell.swift
//  Cat
//
//  Created by Arthit Thongpan on 4/23/17.
//  Copyright © 2017 Arthit Thongpan. All rights reserved.
//

import UIKit

class CateProfileTableViewCell: UITableViewCell {

    @IBOutlet weak var catImageView: UIImageView!
    @IBOutlet weak var cateImageViewHeightConstraint: NSLayoutConstraint!
    
    func setup(path: String, species: Species) {
        catImageView.loadImage(path: path, imageName: species.image!) { (image) in
            self.updateRatio(image: image)
        }
    }
    
    func updateRatio(image: UIImage?) {
        guard let image = image else { return }        
        let imageWidth = catImageView.frame.width
        cateImageViewHeightConstraint.constant =  imageWidth * (image.size.height / image.size.width)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        catImageView.image = nil
    }
}
