//
//  SimpleTableViewCell.swift
//  Cat
//
//  Created by Arthit Thongpan on 5/14/17.
//  Copyright © 2017 Arthit Thongpan. All rights reserved.
//

import UIKit

class SimpleTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}
